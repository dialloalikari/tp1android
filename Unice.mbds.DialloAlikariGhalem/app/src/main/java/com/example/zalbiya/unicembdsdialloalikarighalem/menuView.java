package com.example.zalbiya.unicembdsdialloalikarighalem;

        import android.app.ProgressDialog;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.AsyncTask;
        import android.os.Parcelable;
        import android.support.design.widget.FloatingActionButton;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.support.v7.widget.Toolbar;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;

        import com.example.zalbiya.unicembdsdialloalikarighalem.entity.CommandeItemAdapter;
        import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Produit;
        import com.example.zalbiya.unicembdsdialloalikarighalem.entity.ProduitItemAdapter;

        import org.apache.http.HttpResponse;
        import org.apache.http.client.HttpClient;
        import org.apache.http.client.methods.HttpGet;
        import org.apache.http.impl.client.DefaultHttpClient;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedReader;
        import java.io.InputStreamReader;
        import java.util.ArrayList;

public class MenuView extends AppCompatActivity implements View.OnClickListener{
    private CommandeItemAdapter adapter;
    private ArrayList<Produit> produitMenu;
    private ArrayList<String> itemIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        openOptionsMenu();
        itemIds = getIntent().getStringArrayListExtra("menu");
        System.out.println("Ce que je reçois: " + itemIds);

        new MyTask().execute();

    }

    @Override
    public void onClick(View v) {

    }

    /*****Task*******/

    class MyTask extends AsyncTask<String, String, String> {

        public MyTask() {}

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                HttpGet get = new HttpGet(getString(R.string.url_product));
                response = client.execute(get);

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                return result.toString();
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String theResponse) {
            System.out.println("Réponse: " + theResponse);
            super.onPostExecute(theResponse);
            showProgressDialog(false);
            produitMenu = new ArrayList<>();

            try {

                JSONArray array = new JSONArray(theResponse); //Récupération des utilisateurs

                for (int i = 0; i < array.length(); i++) {
                    try {
                        JSONObject ob = array.getJSONObject(i);
                        if(itemIds.contains(ob.getString("id"))){
                            System.out.println(i +" "+ ob.getString("id"));
                            Produit p = new Produit();
                            p.setId(ob.getString("id"));
                            System.out.println("id: " + ob.getString("id"));
                            p.setName(ob.getString("name"));
                            p.setDescription(ob.getString("description"));
                            p.setPrice(ob.getString("price"));
                            p.setType(ob.getString("type"));
                            p.setDiscount(ob.getString("discount"));
                            p.setUrl_picture(ob.getString("picture"));
                            p.setCalorie(ob.getString("calories"));

                            produitMenu.add(p);
                        }

                    } catch (JSONException e) {
                        System.out.println("Je n'ai pas réussi à récupérer le produit. \n Message erreur: " + e.toString());
                    }

                }

                System.out.println("Affichage du contenu de products");
                for(int i = 0; i < produitMenu.size(); i++) {
                    System.out.println(i + " " + produitMenu.get(i));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
//            Intent intent = new Intent(MenuView.this, CommandeActivity.class);
//            intent.putParcelableArrayListExtra("menu", (ArrayList<? extends Parcelable>) produitMenu);
//            System.out.println(produitMenu.size());
//            startActivity(intent);

            android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.menu);
            adapter = new CommandeItemAdapter(MenuView.this, produitMenu, MenuView.this);
            lst.setAdapter(adapter);

        }
    }
    ProgressDialog progressDialog;
    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

    /*****End Task*******/


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    /*
     select d'un item du menu  à deplacer aussi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.user:
                Intent i = new Intent(this,ListView.class);
                this.startActivity(i);
                return true;
            case R.id.produit:
                Intent produit = new Intent(this, ListProduitActivity.class);
                this.startActivity(produit);
                return true;
            case R.id.menu:
                Intent menu = new Intent(this, ListMenu.class);
                this.startActivity(menu);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
