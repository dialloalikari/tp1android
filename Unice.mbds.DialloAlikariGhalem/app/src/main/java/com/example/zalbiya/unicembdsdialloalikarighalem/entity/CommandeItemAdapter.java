package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.zalbiya.unicembdsdialloalikarighalem.R;

import java.util.List;

/**
 * Created by Zalbiya on 26/12/2015.
 */
public class CommandeItemAdapter extends BaseAdapter {
    Context context;
    List<Produit> produits;
    View.OnClickListener listener;

    public CommandeItemAdapter(Context context, List<Produit> produits, View.OnClickListener listener) {
        this.context = context;
        this.produits = produits;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return produits.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        CommandeViewHolder viewHolder = null;

        if (view == null) {
            view = View.inflate(context, R.layout.template_commande, null);
            viewHolder = new CommandeViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.name);
            viewHolder.type = (TextView) view.findViewById(R.id.type);
            viewHolder.show = (Button) view.findViewById(R.id.show);
            viewHolder.show.setOnClickListener(listener);

            viewHolder.delete = (Button) view.findViewById(R.id.remove);
            viewHolder.delete.setOnClickListener(listener);

            view.setTag(viewHolder);
        } else {
            viewHolder = (CommandeViewHolder) view.getTag();
        }
        Produit produit = produits.get(position);

        viewHolder.show.setTag(produit);
        viewHolder.delete.setTag(produit);
        viewHolder.name.setText(produit.getName());
        viewHolder.type.setText(produit.getType());
        return view;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    class CommandeViewHolder {
        TextView name;
        TextView type;
        Button show;
        Button delete;
    }
}


