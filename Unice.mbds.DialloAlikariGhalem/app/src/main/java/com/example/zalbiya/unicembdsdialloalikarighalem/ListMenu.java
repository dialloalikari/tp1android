package com.example.zalbiya.unicembdsdialloalikarighalem;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Menu;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.MenuItemAdapter;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Produit;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.ProduitItemAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ListMenu extends AppCompatActivity implements View.OnClickListener{

    private MenuItemAdapter adapter;
    ArrayList<Menu> menus = new ArrayList<>();
    ArrayList<String> itemIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new MyTask().execute();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.show) {
            Menu m = (Menu) v.getTag();
            System.out.println("detail tag: " + v.getTag());
            Intent intent = new Intent(ListMenu.this, MenuView.class);
            intent.putStringArrayListExtra("menu", m.getIdsProduits());
            System.out.println("menu: " + m.getIdsProduits());
            startActivity(intent);
        }

    }

    class MyTask extends AsyncTask<String, String, String> {

        public MyTask() {}

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                HttpGet get = new HttpGet(getString(R.string.url_menu));
                response = client.execute(get);

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    System.out.println("line: " + line);
                    result.append(line);
                }

                System.out.println(result.toString());
                return result.toString();
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String theResponse) {
            System.out.println("Réponse: " + theResponse);
            super.onPostExecute(theResponse);
            showProgressDialog(false);

            try {

                JSONArray array = new JSONArray(theResponse); //Récupération des menus

                for (int i = 0; i < array.length(); i++) {
                    try {
                        JSONObject ob = array.getJSONObject(i);
                        Menu m = new Menu();
                        m.setPrice(ob.getString("price"));
                        m.setDiscount(ob.getString("discount"));
                        m.setDateCreation(ob.getString("createdAt"));
                        itemIds = new ArrayList();

                        // On récupère le tableau d'objets qui nous concernent
                        JSONArray ids = new JSONArray(ob.getString("items"));
                        // Pour tous les objets on récupère les infos
                        for (int item = 0; item < ids.length(); item++) {
                            // On récupère un objet JSON du tableau
                            JSONObject id = new JSONObject(ids.getString(item));
                            // On ajoute l'id à la liste
                            itemIds.add(id.getString("id"));
                        }
                        System.out.println("la liste des ids: " + itemIds);

                        m.setIdsProduits(itemIds);
                        JSONObject idCooker = new JSONObject(ob.getString("cooker"));
                        m.setCooker(idCooker.getString("id"));
                        JSONObject idServer = new JSONObject(ob.getString("server"));
                        m.setServer(idServer.getString("id"));

                        menus.add(m);
                    } catch (JSONException e) {
                        System.out.println("Message erreur: " + e.toString());
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_menu);
            adapter = new MenuItemAdapter(ListMenu.this, menus, ListMenu.this);
            lst.setAdapter(adapter);

        }
    }
    ProgressDialog progressDialog;
    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    /*
     select d'un item du menu  à deplacer aussi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.user:
                Intent i = new Intent(this,ListView.class);
                this.startActivity(i);
                return true;
            case R.id.produit:
                Intent produit = new Intent(this, ListProduitActivity.class);
                this.startActivity(produit);
                return true;
            case R.id.menu:
                Intent menu = new Intent(this, ListMenu.class);
                this.startActivity(menu);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
