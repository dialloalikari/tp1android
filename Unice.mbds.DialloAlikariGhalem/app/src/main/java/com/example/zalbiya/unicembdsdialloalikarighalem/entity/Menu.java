package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hasso on 10/12/2015.
 */
public class Menu {

    private ArrayList idsProduits = new ArrayList();
    private String price;
    private String discount;
    private String cooker;
    private String server;
    private String dateCreation;

    public Menu(){}

    public Menu(ArrayList idsProduits,  String price, String discount, String cooker, String server, String dateCreation) {
        this.idsProduits = idsProduits;
        this.price = price;
        this.discount = discount;
        this.cooker = cooker;
        this.server = server;
        this.dateCreation  = dateCreation;
    }


    public ArrayList getIdsProduits() {
        return idsProduits;
    }

    public void setIdsProduits(ArrayList idsProduits) {
        this.idsProduits = idsProduits;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCooker() {
        return cooker;
    }

    public void setCooker(String cooker) {
        this.cooker = cooker;
    }
}
