package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.zalbiya.unicembdsdialloalikarighalem.R;

import java.util.List;

/**
 * Created by Zalbiya on 30/10/2015.
 */

public class PersonItemAdapter extends BaseAdapter {
    private Context context;
    public List<Person> person;
    //PersonAdapter n'est pas fais pour traiter des requetes json donc on recupère notre button et on lui passe une instante de onclickListner
    //listner est notre onclick que l'on passe a notre button.
    /*
      Etapes pour à faire pour réussir tout ça:
         Dans listView.java
            1-ajouter implements onclickListner
            2-Implementer la méthode setOnclick
            3- On recupère la personne à suprimer dans setOnclick avec getTag();


         Dans personAdapter
             1-Récuperer une instance du listener
             2- ajouter le listener dans le constructeur
             3-recupère le button delete  et on lui passe le listener
             4-Pour passer la personne à supprimer  on utilise setTag(personne) pour la récupere dans la methode setOnClick avec getTag


     */
    public List<Person> getPerson(){
        return this.person;
    }
    public View.OnClickListener listener;

    public PersonItemAdapter(Context context, List<Person> person, View.OnClickListener listener) {
        this.context = context;
        this.person = person;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return person.size();
    }

    @Override
    public Object getItem(int arg0) {
        return person.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        View v = convertView;

        PersonViewHolder viewHolder = null;
        if (v == null) {
            v = View.inflate(context, R.layout.template_person, null);
            viewHolder = new PersonViewHolder();
            viewHolder.nom_prenom = (TextView) v.findViewById(R.id.nom_prenom_utilisateur);
            viewHolder.buzzer = (TextView) v.findViewById(R.id.buzzerText);
            viewHolder.status = (TextView) v.findViewById(R.id.status);
            //Ici on recupère le button delete  et on lui passe le listener (c'est comme si on faisait button.setonOncli.......
            viewHolder.delete =(Button) v.findViewById(R.id.delete);
            viewHolder.delete.setOnClickListener(listener);

            v.setTag(viewHolder);
        } else {
            viewHolder = (PersonViewHolder) v.getTag();
        }
        Person personne = person.get(position);
        //ici on passe la personne à suprrimer au listenre
        viewHolder.delete.setTag(personne);
        viewHolder.nom_prenom.setText(personne.getNom() + " " + personne.getPrenom());
        viewHolder.status.setText(personne.getStatus());
            if (personne.isConnected()) {
                viewHolder.buzzer.setCompoundDrawablesWithIntrinsicBounds(null, context.getDrawable(R.drawable.mybutton), null, null);
            } else {
                viewHolder.buzzer.setCompoundDrawablesWithIntrinsicBounds(null, context.getDrawable(R.drawable.mybuttonoffline), null, null);
            }
            return v;
        }



    class PersonViewHolder {
        TextView nom_prenom;
        TextView status;
        TextView buzzer;
        //Ici on declare un button pour récuperer notre click
        Button delete;
    

    }
}

