package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zalbiya.unicembdsdialloalikarighalem.R;

import java.util.List;

/**
 * Created by hasso on 12/12/2015.
 */
public class MenuItemAdapter extends BaseAdapter {
    Context context;
    List<Menu> menus;
    View.OnClickListener listener;

    public MenuItemAdapter(Context context, List<Menu> menus, View.OnClickListener listener) {
        this.context = context;
        this.menus = menus;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return menus.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
       MenuViewHolder viewHolder = null;

        if (view == null) {
            view = View.inflate(context, R.layout.template_menu, null);

            viewHolder = new MenuViewHolder();
            viewHolder.date = (TextView) view.findViewById(R.id.date);
            viewHolder.server = (TextView) view.findViewById(R.id.server);
            viewHolder.cooker = (TextView) view.findViewById(R.id.cooker);

            viewHolder.show = (Button) view.findViewById(R.id.show);
            viewHolder.show.setOnClickListener(listener);

            view.setTag(viewHolder);
        } else {
            viewHolder = (MenuViewHolder) view.getTag();
        }
        Menu menu = menus.get(position);
        viewHolder.show.setTag(menu);
        viewHolder.date.setText(menu.getDateCreation());
        viewHolder.server.setText("server: " + menu.getServer());
        viewHolder.cooker.setText("cooker: " + menu.getCooker());
        return view;
    }

    class MenuViewHolder {
        TextView date;
        TextView server;
        TextView cooker;
        Button show;
    }

}
