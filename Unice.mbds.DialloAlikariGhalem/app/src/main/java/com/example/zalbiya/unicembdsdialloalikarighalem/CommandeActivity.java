package com.example.zalbiya.unicembdsdialloalikarighalem;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Commande;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.CommandeItemAdapter;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Produit;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CommandeActivity extends AppCompatActivity implements View.OnClickListener {

    private Produit p;
    private CommandeItemAdapter adapter;
    private ArrayList<Produit> commandeList;
    Commande cmd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commande);
        commandeList = getIntent().getParcelableArrayListExtra("commande");
        System.out.println("Id dans commandeActivity: " +  commandeList);
        for(int i = 0; i < commandeList.size(); i++) {
            System.out.println(i + ": " + commandeList.get(i).getId() + ", " + commandeList.get(i).getPrice());
        }
        cmd = new Commande(commandeList);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        System.out.println("taille de la liste commande: " + commandeList.size());

        TextView montant = (TextView) findViewById(R.id.montant);
        montant.setText(cmd.getPrice() - cmd.getReduction() + "");
        android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.commande);
        adapter = new CommandeItemAdapter(CommandeActivity.this, commandeList, CommandeActivity.this);
        lst.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cmd.getPrice();
                cmd.getReduction();
                cmd.getIds();
                new MyTask().execute();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.show) {
            System.out.println("test 0 ");
            p = (Produit) v.getTag();
            System.out.println("detail tag: " + v.getTag());
            Intent intent = new Intent(CommandeActivity.this, Product.class);
            intent.putExtra("produit", p);
            startActivity(intent);
        } else if (v.getId() == R.id.remove) {
            System.out.println("test1 ");
            p =(Produit)v.getTag();
            AlertDialog.Builder adb=new AlertDialog.Builder(CommandeActivity.this);
            adb.setTitle("Supprimer");
            adb.setMessage("Êtes vous sur de vouloir supprimer " + p.getName() + "?");
            adb.setNegativeButton("Annuler", null);
            adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    int position = adapter.getProduits().indexOf(p);
                    adapter.getProduits().remove(position);
                    adapter.notifyDataSetChanged();
                    commandeList.remove(p);

                    System.out.println("Suppresion réuissi ");
                    for (int i = 0; i < commandeList.size(); i++) {
                        System.out.println("ma liste nouvelle  :" + p.getName());
                    }
                }
            });
            adb.show();
        } else if (v.getId() == R.id.show) {
            System.out.println("test 2 ");
            p = (Produit) v.getTag();
            System.out.println("liste produit: " + v.getTag());
            Intent intent = new Intent(CommandeActivity.this, ListProduitActivity.class);
            intent.putExtra("produit", p);
            startActivity(intent);
        }
    }




    class MyTask extends AsyncTask<String, String, String> {

        public MyTask() {}
        @Override
        //Où s'exécute la requête Http
        protected String doInBackground(String... voids) {
            System.out.println("Je suis rentrée dans le doInBackground");
            try{
                System.out.println("try");
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(getString(R.string.url_menu));
                Commande commande = new Commande(commandeList);

                // add header

                post.setHeader("Content-Type", "application/json");
                JSONObject obj = new JSONObject();
                JSONObject objServer = new JSONObject();
                JSONObject objCooker = new JSONObject();
                JSONObject objIds = new JSONObject();
                JSONArray idArray = new JSONArray();

                objServer.put("id", commande.getServer());
                objCooker.put("id", commande.getCooker());
                System.out.println("J'ai crée mon objett json 'obj'");
                obj.put("price", commande.getPrice());
                obj.put("discount", Math.round(commande.getReduction()*100.0)/100.0);
                obj.put("server", objServer);
                obj.put("cooker", objCooker);
                for(int i = 0; i < commandeList.size(); i++) {
                    idArray.put(objIds.put("id", commandeList.get(i).getId()));
                }
                obj.put("items", idArray);
                System.out.println("put: " + obj.toString());
//                .....
                StringEntity entity = new StringEntity(obj.toString());

                post.setEntity(entity);

                HttpResponse response = client.execute(post);
                System.out.println("Post parameters : " + post.getEntity());
                System.out.println("Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println("result: " + result.toString());
                return result.toString();
            } catch (Exception e){
                System.out.println("L'erreur est: " + e);
            }
            return null;
        }


        @Override
        //S'éxécute après le thread (doInBackground) où s'éxécute la requête Http
        protected void onPostExecute(String theResponse) {
            System.out.println("Je suis rentrée dans le onPostExecute");
            super.onPostExecute(theResponse);
            showProgressDialog(false);
            Toast.makeText(CommandeActivity.this, R.string.envoi_ok, Toast.LENGTH_LONG).show(); //le pop up
            startActivity(new Intent(CommandeActivity.this, ListProduitActivity.class));
        }
    }

    ProgressDialog progressDialog;
    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(this.getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    /*
     select d'un item du menu  à deplacer aussi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.user:
                Intent i = new Intent(this,ListView.class);
                this.startActivity(i);
                return true;
            case R.id.produit:
                Intent produit = new Intent(this, ListProduitActivity.class);
                this.startActivity(produit);
                return true;
            case R.id.menu:
                Intent menu = new Intent(this, ListMenu.class);
                this.startActivity(menu);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
