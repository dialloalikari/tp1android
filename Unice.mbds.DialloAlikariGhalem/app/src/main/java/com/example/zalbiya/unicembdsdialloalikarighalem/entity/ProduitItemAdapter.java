package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zalbiya.unicembdsdialloalikarighalem.R;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Zalbiya on 09/12/2015.
 */

public class ProduitItemAdapter extends BaseAdapter {
    Context context;
    List<Produit> produits;
    View.OnClickListener listener;

    public ProduitItemAdapter(Context context, List<Produit> produits, View.OnClickListener listener) {
        this.context = context;
        this.produits = produits;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return produits.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ProductViewHolder viewHolder = null;

        if (view == null) {
            view = View.inflate(context, R.layout.template_products, null);
            viewHolder = new ProductViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.nom_produit);
           // viewHolder.pic = (ImageView) view.findViewById(R.id.picProduct);
            viewHolder.type = (TextView) view.findViewById(R.id.type_produit);

            viewHolder.detail = (Button) view.findViewById(R.id.detail);
            viewHolder.detail.setOnClickListener(listener);

            viewHolder.add = (Button) view.findViewById(R.id.add_from_list);
            viewHolder.add.setOnClickListener(listener);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ProductViewHolder) view.getTag();
        }
        Produit produit = produits.get(position);

       /* viewHolder.pic.setImageResource(R.drawable.ic_phone_24dp);
        new DownloadImageTask(viewHolder.pic).execute(produit.getUrl_picture());*/

        viewHolder.detail.setTag(produit);
        viewHolder.add.setTag(produit);
        viewHolder.name.setText(produit.getName());
        viewHolder.type.setText(produit.getType());
        return view;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }


    class ProductViewHolder {
        TextView name;
        TextView type;
        ImageView pic;
        Button detail;
        Button add;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }
}}