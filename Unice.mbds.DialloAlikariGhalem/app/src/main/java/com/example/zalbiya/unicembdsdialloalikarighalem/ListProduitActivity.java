package com.example.zalbiya.unicembdsdialloalikarighalem;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Commande;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Produit;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.ProduitItemAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ListProduitActivity extends AppCompatActivity implements View.OnClickListener {

    private Produit p;
    private Commande c;
    ProduitItemAdapter adapter;
    private List<Produit> products;
    private List<Produit> all_entrees;
    private List<Produit> all_plats;
    private List<Produit> all_apperitifs;
    private List<Produit> all_desserts;
    private ArrayList<Produit> commande = new ArrayList<>();
    private Button entree;
    private Button plat;
    private Button apperitif;
    private Button dessert;
    private Button all_prodcuts;
    private Button menu_commande;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        openOptionsMenu();

        new MyTask().execute();

        entree = (Button) findViewById(R.id.entree);
        entree.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                all_entrees = getEntrees(products);
                android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_products);
                adapter = new ProduitItemAdapter(ListProduitActivity.this, all_entrees, ListProduitActivity.this);
                lst.setAdapter(adapter);
            }
        });

        plat = (Button) findViewById(R.id.plat);
        plat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                all_plats = getPlats(products);
                android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_products);
                adapter = new ProduitItemAdapter(ListProduitActivity.this, all_plats, ListProduitActivity.this);
                lst.setAdapter(adapter);
            }
        });

        apperitif = (Button) findViewById(R.id.apperitif);
        apperitif.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                all_apperitifs = getApperitifs(products);
                android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_products);
                adapter = new ProduitItemAdapter(ListProduitActivity.this, all_apperitifs, ListProduitActivity.this);
                lst.setAdapter(adapter);
            }
        });

        dessert = (Button) findViewById(R.id.dessert);
        dessert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                all_desserts = getDesserts(products);
                android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_products);
                adapter = new ProduitItemAdapter(ListProduitActivity.this, all_desserts, ListProduitActivity.this);
                lst.setAdapter(adapter);
            }
        });

        all_prodcuts = (Button) findViewById(R.id.all_products);
        all_prodcuts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                all_entrees = getEntrees(products);
                android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_products);
                adapter = new ProduitItemAdapter(ListProduitActivity.this, products, ListProduitActivity.this);
                lst.setAdapter(adapter);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListProduitActivity.this, CommandeActivity.class);
                intent.putParcelableArrayListExtra("commande", (ArrayList<? extends Parcelable>) commande);
                System.out.println(commande.size());
                startActivity(intent);

            }
        });
    }

    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    /*
       Creation du menuView de l'application à voir ou il faut le deplacer
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    /*
     select d'un item du menu  à deplacer aussi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.user:
                Intent i = new Intent(this,ListView.class);
                this.startActivity(i);
                return true;
            case R.id.produit:

                return true;
            case R.id.menu:
                Intent menu = new Intent(this, ListMenu.class);
                this.startActivity(menu);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Récupérer les entrées
    public List<Produit> getEntrees(List<Produit> produits) {
        List<Produit> entrees = new ArrayList<>();
        for(int i = 0; i < produits.size(); i++) {
            if (produits.get(i).getType().equals("Entrée")) {
                Produit p = produits.get(i);
                entrees.add(p);
            }
        }
        return entrees;
    }

    //Récupérer les plats
    public List<Produit> getPlats(List<Produit> produits) {
        List<Produit> plats = new ArrayList<>();
        for(int i = 0; i < produits.size(); i++) {
            if (produits.get(i).getType().equals("Plat ")) {
                Produit p = produits.get(i);
                plats.add(p);
            }
        }
        return plats;
    }

    //Récupérer les appéritifs
    public List<Produit> getApperitifs(List<Produit> produits) {
        List<Produit> apperitifs = new ArrayList<>();
        for(int i = 0; i < produits.size(); i++) {
            if (produits.get(i).getType().equals("Appéritif")) {
                Produit p = produits.get(i);
                apperitifs.add(p);
            }
        }
        return apperitifs;
    }

    //Récupérer les desserts
    public List<Produit> getDesserts(List<Produit> produits) {
        List<Produit> desserts = new ArrayList<>();
        for(int i = 0; i < produits.size(); i++) {
            if (produits.get(i).getType().equals("Dessert")) {
                Produit p = produits.get(i);
                desserts.add(p);
            }
        }
        return desserts;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.detail) {
            p = (Produit) v.getTag();
            System.out.println("detail tag: " +  v.getTag());
            Intent intent = new Intent(ListProduitActivity.this, Product.class);
            intent.putExtra("produit", p);
            startActivity(intent);
        } else if(v.getId() == R.id.add_from_list) {
            System.out.println("On ajoute le produit à la liste de commande");
            p = (Produit) v.getTag();
            System.out.println("add tag: " +  v.getTag());
            System.out.println("Nom: " + p.getName() + ", id: " + p.getId());
            commande.add(p);
            Toast.makeText(ListProduitActivity.this, R.string.add_ok, Toast.LENGTH_LONG).show();
        }

        for(int i = 0; i < commande.size(); i++) {
            System.out.println(commande.get(i).getName());
        }
    }


    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

    class MyTask extends AsyncTask<String, String, String> {

        public MyTask() {}

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                HttpGet get = new HttpGet(getString(R.string.url_product));
                response = client.execute(get);

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    System.out.println("line: " + line);
                    result.append(line);
                }

                System.out.println(result.toString());
                return result.toString();
            } catch (Exception e) {

            }
            return null;
        }

//        @Override
//        protected String doInBackground(String... params) {
//            String test1 = "[{name: \"Black & Basmati\", description: \"Lorem ipsum quet bibendum aliquet dictumst. Ac duis tempus molestie viverra adipiscing libero posuere mollis dapibus mattis suscipit. Nostra iaculis leo lacus lorem malesuada curae; facilisi molestie Mattis felis rhoncus fusce tortor maecenas curabitur, risus at interdum.\\nAliquet dapibus per facilisis magnis quisque nunc nascetur sapien ac dictum.\\nJusto ultricies cum aenean class dictumst class cubilia lorem elit eros.\\nRidiculus, aptent hendrerit ligula vel nascetur habitasse diam himenaeos augue\\n\" ,price: 107, calories: 174, type: \"Plat \", picture: \"http://placehold.it/540x303\", discount: 17, createdAt: \"2015-11-09T21:44:15.661Z\", updatedAt: \"2015-11-09T21:44:15.661Z\", id: \"564113af6c7e6511333f615d\"}," +
//                    "{name: \"Yellow Banana\", description: \"Lorem ipsum morbi habitant imperdiet cum scelerisque rhoncus mattis aliqu dictumst. Ac duis tempus molestie viverra adipiscing libero posuere mollis dapibus mattis suscipit. Mattis felis rhoncus fusce tortor maecenas curabitur, risus at interdum. Justo ultricies cum aenean class dictumst class cubilia lorem elit eros.\\nRidiculus, aptent hendrerit ligula vel nascetur habitasse diam himenaeos augue.\\nImperdiet cum platea curabitur, amet pretium sociosqu varius risus.\\nNibh elementum pretium condimentum ut egestas magna nam torquent hac eros\\n\",price: 67, calories: 174, type: \"Dessert\", picture: \"http://placehold.it/540x303\", discount: 1, createdAt: \"2015-11-09T21:44:15.661Z\", updatedAt: \"2015-11-09T21:45:15.661Z\", id: \"564113af6c7e6511333f616d\"}," +
//                    "{name: \"White & Ice cream\", description: \"Lorem ipsum morbi habitant imperdiet cum scelerisque rhoncus mattis aliquet bibendum aliquet dictumst. Nostra  molestie. Mattis felis rhoncus fusce tortor maecenas curabitur, risus at interdum. Aliquet dapibus per facilisis magnis quisque nunc nascetur sapien ac dictum.\\nJusto ultricies cum aenean class dictumst class cubilia lorem elit eros.\\nRidiculus, aptent hendrerit ligula vel nascetur habitasse diam himenaeos augue.\\nNibh elementum pretium condimentum ut egestas magna nam torquent hac eros.\\nFringilla accumsan a facilisis dapibus commodo sociis aliquet vel rhoncus convallis.\\n\",price: 67, calories: 174, type: \"Entrée\", picture: \"http://placehold.it/540x303\", discount: 50, createdAt: \"2015-11-09T21:44:15.661Z\", updatedAt: \"2015-11-09T21:44:15.661Z\", id: \"564113af6c7e6511333f617d\"}," +
//                    "{name: \"Pink & Pasta\",description: \"Lorem ipsum morbi habitant imperdiet cum scelerisque rhoncus mattis aliquet bibendum aliquet dictumst. Ac duis tempus molestie viverra adipiscing libero posuere mollis dapibus mattis suscipit.\\nAliquet dapibus per facilisis magnis quisque nunc nascetur sapien ac dictum.\\nJusto ultricies cum aenean class dictumst class cubilia lorem elit eros.\\nImperdiet cum platea curabitur, amet pretium sociosqu varius risus.\\nFringilla accumsan a facilisis dapibus commodo sociis aliquet vel rhoncus convallis.\\n\",price: 67, calories: 74, type: \"Appéritif\", picture: \"http://placehold.it/540x303\", discount: 1, createdAt: \"2015-11-09T21:44:15.661Z\", updatedAt: \"2015-11-09T21:44:15.661Z\", id: \"564113af6c7e6511333f613d\"}," +
//                    "{name: \"Green & Epinard\", description: \"Facilisi molestie Mattis felis rhoncus fusce tortor maecenas curabitur, risus at interdum.\\nAliquet dapibus per facilisis magnis quisque nunc nascetur sapien ac dictum.\\nJusto ultricies cum aenean class dictumst class cubilia lorem elit eros.\\nRidiculus, aptent hendrerit ligula vel nascetur habitasse diam himenaeos augue\\n\" ,price: 87, calories: 14, type: \"Plat \", picture: \"http://placehold.it/540x303\", discount: 57, createdAt: \"2015-11-09T21:44:15.661Z\", updatedAt: \"2014-11-09T21:44:15.661Z\", id: \"564113af6r7e6511333f615d\"}," +
//                    "{name: \"Blue & :-)\",description: \"Lorem ipsum morbi habitant imperdiet cum scelerisque rhoncus mattis aliquet bibendum aliquet dictumst. Ac duis tempus molestie viverra adipiscing libero posuere mollis dapibus mattis suscipit.\\nJusto ultricies cum aenean class dictumst class cubilia lorem elit eros.\\nImperdiet cum platea curabitur, amet pretium sociosqu varius risus.\\nFringilla accumsan a facilisis dapibus commodo sociis aliquet vel rhoncus convallis.\\n\",price: 71, calories: 4, type: \"Appéritif\", picture: \"http://placehold.it/540x303\", discount: 1, createdAt: \"2015-11-09T21:44:15.661Z\", updatedAt: \"2015-11-09T21:24:15.661Z\", id: \"564113af6c7e9511333f613d\"} ]";
//            return test1;
//        }

        @Override
        protected void onPostExecute(String theResponse) {
            System.out.println("Réponse: " + theResponse);
            super.onPostExecute(theResponse);
            showProgressDialog(false);
            products = new ArrayList<>();

                try {

                    JSONArray array = new JSONArray(theResponse); //Récupération des utilisateurs

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject ob = array.getJSONObject(i);
                            Produit p = new Produit();
                            p.setId(ob.getString("id"));
                            System.out.println("id: " + ob.getString("id"));
                            p.setName(ob.getString("name"));
                            p.setDescription(ob.getString("description"));
                            p.setPrice(ob.getString("price"));
                            p.setType(ob.getString("type"));
                            p.setDiscount(ob.getString("discount"));
                            p.setUrl_picture(ob.getString("picture"));
                            p.setCalorie(ob.getString("calories"));

                            products.add(p);

                        } catch (JSONException e) {
                            System.out.println("Je n'ai pas réussi à récupérer le produit. \n Message erreur: " + e.toString());
                        }

                    }

                    System.out.println("Affichage du contenu de products");
                    for(int i = 0; i < products.size(); i++) {
                        System.out.println(i + " " + products.get(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.list_products);
            adapter = new ProduitItemAdapter(ListProduitActivity.this, products, ListProduitActivity.this);
            lst.setAdapter(adapter);

        }
    }
    ProgressDialog progressDialog;
    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

}
