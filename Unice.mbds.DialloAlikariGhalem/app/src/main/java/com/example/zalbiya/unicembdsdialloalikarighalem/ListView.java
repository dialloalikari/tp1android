package com.example.zalbiya.unicembdsdialloalikarighalem;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Person;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.PersonItemAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ListView extends AppCompatActivity implements View.OnClickListener {

    private  Person p;
    PersonItemAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

          new MyTask(0).execute(); //Au click on crée la liste des utilisateurs

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListView.this, RegisterActivity.class);
                startActivity(i);
            }
        });
    }
    /*
        Creation mdu menuView de l'application à voir ou il faut le deplacer
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    /*
     select d'un item du menue  à deplacer aussi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.user:

                return true;
            case R.id.produit:
                Intent ii = new Intent(this,ListProduitActivity.class);
                this.startActivity(ii);
                return true;
            case R.id.menu:
                Intent menu = new Intent(this, ListMenu.class);
                this.startActivity(menu);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onClick(View view) {
         p =(Person)view.getTag();
                AlertDialog.Builder adb=new AlertDialog.Builder(ListView.this);
                adb.setTitle("Supprimer");
                adb.setMessage("Êtes vous sur de vouloir supprimer " + p.getNom() +"?");
                //final int positionToRemove = position;
                adb.setNegativeButton("Annuler", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new MyTask(1).execute();

                    }});
                adb.show();


        System.out.println("bouton delete cliqué sur la personne  :" +p.getNom());
    }



    class MyTask extends AsyncTask<String, String, String> {
        int action;

        public MyTask(int action) {
            this.action = action;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                if (action == 0) {
                    HttpGet get = new HttpGet(getString(R.string.url));
                    response = client.execute(get);
                } else {
                    HttpDelete delete = new HttpDelete(getString(R.string.url) + "" + p.getId());
                    //delete.setHeader(HTTP.CONTENT_TYPE, "text/xml");
                    System.out.println("url : "+getString(R.string.url) + "" + p.getId());
                    response = client.execute(delete);
                }
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println(result.toString());
                return result.toString();
            } catch (Exception e) {

            }
            return null;

        }

        @Override
        protected void onPostExecute(String theResponse) {
            super.onPostExecute(theResponse);
            showProgressDialog(false);
            android.widget.ListView lst = (android.widget.ListView) findViewById(R.id.listView);
            ArrayList<Person> person = new ArrayList<>();

            if (action == 0) {
                try {

                    JSONArray array = new JSONArray(theResponse); //Récupération des utilisateurs

                    for (int i = 0; i < array.length(); i++) {
                        try {

                            System.out.println("J'essaie de récupérer un utilisateur");
                            JSONObject ob = array.getJSONObject(i);
                            Person p = new Person();
                            p.setNom(ob.getString("nom"));
                            p.setPrenom(ob.getString("prenom"));
                            p.setEmail(ob.getString("email"));
                            p.setPass(ob.getString("password"));
                            p.setTelephone(ob.getString("telephone"));
                            p.setGenre(ob.getString("sexe"));
                            p.setId(ob.getString("id"));

                            if (ob.getString("connected") == "true") {
                                p.setConnected(true);
                            } else {
                                p.setConnected(false);
                            }
                            System.out.println("J'ai réussi la récupération de l'utilisateur de " + p.getNom() + " " + p.getPrenom());

                            person.add(p);

                        } catch (JSONException e) {
                            System.out.println("Je n'ai pas réussi à récupérer l'utilisateur. \n Message erreur: " + e.toString());
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("personnes: " + person);
                 adapter = new PersonItemAdapter(ListView.this, person, ListView.this);
                lst.setAdapter(adapter);

            } else {
                int position = adapter.getPerson().indexOf(p);
                adapter.getPerson().remove(position);
                adapter.notifyDataSetChanged();
                System.out.println("Suppresion réuissi ");
            }

        }
    }
    ProgressDialog progressDialog;
    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

}
