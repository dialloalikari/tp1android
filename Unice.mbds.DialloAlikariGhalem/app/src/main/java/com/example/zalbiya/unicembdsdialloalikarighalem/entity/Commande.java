package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MBDS on 22/12/2015.
 */
public class Commande {

    private long server;
    private long cooker;
    private List<Produit> produits;

    public Commande(List<Produit> produits) {
        this.produits = produits;
    }

    public float getPrice() {
        float price = 0;
        for(int i = 0; i < produits.size(); i++) {
            price += Float.parseFloat(produits.get(i).getPrice());
            System.out.println("prix produit " + produits.get(i).getPrice());
            System.out.println("somme prix " + i + "  " + price);
        }
        System.out.println("prix final: " + price);
        return price;
    }

    public float getReduction() {
        float reduction = 0;
        for(int i = 0; i < produits.size(); i++) {
            System.out.println("Prix: " + produits.get(i).getPrice() + " Réduction: " + produits.get(i).getDiscount());
            System.out.println("Montant de la réduction: " + (Float.parseFloat(produits.get(i).getPrice()) * Float.parseFloat(produits.get(i).getDiscount()) / 100));
            reduction += (Float.parseFloat(produits.get(i).getPrice()) * Float.parseFloat(produits.get(i).getDiscount()) / 100);
            System.out.println("Réduction somme: " + reduction);
        }
        System.out.println("Réduction final: " + reduction);
        return reduction;
    }

    public ArrayList<String> getIds() {
        ArrayList<String> ids = new ArrayList<String>();
        for(int i = 0; i < produits.size(); i++) {
            ids.add(produits.get(i).getId());
            System.out.println("getIds " + i + " " + produits.get(i).getId());
        }
        return ids;
    }

    public long getServer() {
        return server;
    }

    public void setServer(long server) {
        this.server = server;
    }

    public long getCooker() {
        return cooker;
    }

    public void setCooker(long cooker) {
        this.cooker = cooker;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }
}
