package com.example.zalbiya.unicembdsdialloalikarighalem;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Produit;
import com.example.zalbiya.unicembdsdialloalikarighalem.entity.ProduitItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class Product extends AppCompatActivity  {
    private Produit p;
    TextView name;
    TextView description;
    TextView price;
    TextView calorie;
    TextView type;
    TextView discount;
    ImageView picture;
    Button add;
    ArrayList<Produit> commande = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Produit produit = getIntent().getExtras().getParcelable("produit");

        name = (TextView)findViewById(R.id.name);
        description = (TextView)findViewById(R.id.description);
        price = (TextView)findViewById(R.id.price);
        calorie = (TextView)findViewById(R.id.calorie);
        type = (TextView)findViewById(R.id.type);
        discount = (TextView)findViewById(R.id.discount);
        picture = (ImageView)findViewById(R.id.picture);
        add = (Button)findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                System.out.println("On ajoute le produit à la liste de commande");
//                p = (Produit) v.getTag();
//                System.out.println("Nom: " + p.getName() + ", type: " + p.getType());
//                commande.add(p);
//                Intent intent = new Intent(Product.this, ListProduitActivity.class);
//
//                Bundle bundle_seconde = new Bundle();
//                bundle_seconde.putParcelableArrayList("mylist1", commande);
//                intent.putExtras(bundle_seconde);
            }
        });

        name.setText(produit.getName());
        description.setText(produit.getDescription());
        price.setText("Prix: " + produit.getPrice() + "€");
        calorie.setText("Calorie: " + produit.getCalorie());
        type.setText(produit.getType());
        discount.setText("-" + produit.getDiscount() + "%");
        try {
//            picture.setImageBitmap(getBitmapFromURL(produit.getUrl_picture()));
//            picture.setImageDrawable(loadImageFromWebOperations(produit.getUrl_picture()));
        } catch (Exception e) {
            System.out.println("Erreur: " + e);
        }



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    /*
     select d'un item du menu  à deplacer aussi
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.user:
                Intent i = new Intent(this,ListView.class);
                this.startActivity(i);
                return true;
            case R.id.produit:
                Intent produit = new Intent(this, ListProduitActivity.class);
                this.startActivity(produit);
                return true;
            case R.id.menu:
                Intent menu = new Intent(this, ListMenu.class);
                this.startActivity(menu);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
