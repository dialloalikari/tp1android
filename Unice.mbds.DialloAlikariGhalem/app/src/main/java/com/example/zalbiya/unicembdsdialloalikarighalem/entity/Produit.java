package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zalbiya on 09/12/2015.
 */
public class Produit implements Parcelable {
    private String id;
    private String name;
    private String description;
    private String price;
    private String calorie;
    private String type;
    private String url_picture;
    private String discount;

    public Produit() {

    }

    public Produit(String id, String name, String description, String price, String calorie, String type, String url_picture, String discount) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.calorie = calorie;
        this.type = type;
        this.url_picture = url_picture;
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getUrl_picture() {
        return url_picture;
    }

    public void setUrl_picture(String url_picture) {
        this.url_picture = url_picture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    //Le code qui suit permet de passer à une intention un objet
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(calorie);
        dest.writeString(type);
        dest.writeString(url_picture);
        dest.writeString(discount);
    }

    public static final Parcelable.Creator<Produit> CREATOR = new Parcelable.Creator<Produit>()
    {

        @Override
        public Produit createFromParcel(Parcel source) {
            return new Produit(source);
        }

        @Override
        public Produit[] newArray(int size) {
            return new Produit[size];
        }
    };

    public Produit(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.price = in.readString();
        this.calorie = in.readString();
        this.type = in.readString();
        this.url_picture = in.readString();
        this.discount = in.readString();
    }
}
