package com.example.zalbiya.unicembdsdialloalikarighalem.entity;

import android.widget.EditText;
import android.widget.RadioGroup;

/**
 * Created by Zalbiya on 27/10/2015.
 */
public class Person {
    private String nom;
    private String prenom;
    private String email;
    private String telephone;
    private String pass;
    private String genre;
    private String creator;
    private String pays_origine;
    private String id;

    private boolean connected;


    public Person(){
    }

    public Person(String nom, String prenom, String email, String telephone, String pass, String genre, String creator) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.pass = pass;
        this.genre = genre;
        this.creator=creator;
//        this.pays_origine =pays_origine;
    }


    public Person(String nom, String prenom, String email, String telephone, String pass, String genre, String creator, boolean status,String id) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.pass = pass;
        this.genre = genre;
        this.creator=creator;
        this.id=id;

    }

    public void setId(String id){
        this.id=id;
    }

    public String getId(){
        return id;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean isConnected() {
        return connected;
    }
    
    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public  String getStatus(){
        if(connected) {
            return "Connecté";
        } else {
            return "Déconnecté";
        }
    }
}
