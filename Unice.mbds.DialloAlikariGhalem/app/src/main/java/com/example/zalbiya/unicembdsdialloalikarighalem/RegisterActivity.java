package com.example.zalbiya.unicembdsdialloalikarighalem;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.zalbiya.unicembdsdialloalikarighalem.entity.Person;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RegisterActivity extends AppCompatActivity {

    Person me;
    EditText nom;
    EditText prenom;
    EditText email;
    EditText telephone;
    EditText pass;
    EditText confirm_pass;
    RadioGroup genre;
    Spinner pays_origine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Context context = this.getApplicationContext();
        nom = (EditText) findViewById(R.id.nom);
        prenom = (EditText) findViewById(R.id.prenom);
        email = (EditText) findViewById(R.id.email);
        telephone = (EditText) findViewById(R.id.phone);
        pass = (EditText) findViewById(R.id.pass);
        confirm_pass = (EditText)findViewById(R.id.confirm_pass);
        genre = (RadioGroup) findViewById(R.id.radio_sex); //Le radioGroup récupère l'id du radioButton sélectionné.
        pays_origine =(Spinner) findViewById(R.id.pays_origine);

        Button register = (Button) findViewById(R.id.button);
        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String sexe;
                switch (genre.getCheckedRadioButtonId()) { //Donne l'id sélectionné
                    case R.id.femme:
                        sexe = "F";
                        break;
                    default:
                        sexe = "M";
                }
                if (!TextUtils.isEmpty(nom.getText().toString()) && !TextUtils.isEmpty(prenom.getText().toString() ) && !TextUtils.isEmpty(email.getText().toString()) && !TextUtils.isEmpty(telephone.getText().toString())&& !TextUtils.isEmpty(pass.getText().toString())
                        && !TextUtils.isEmpty(sexe)) {
                    if(!pass.getText().toString().equals(confirm_pass.getText().toString())) {
                        System.out.println("Les mots de passe doivent être identique!");
                    } else {
                        me = new Person(nom.getText().toString(), prenom.getText().toString(), email.getText().toString(),
                                telephone.getText().toString(), pass.getText().toString(), sexe, prenom.getText().toString());
                        new MyTask().execute(); //Au click on crée
                    }
                } else {
                    System.out.println("Tous les champs sont requis!!!");
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Button monButton = (Button) findViewById(R.id.button);
        monButton.setBackgroundColor(Color.BLUE);
        String[]coutries = getResources().getStringArray(R.array.country_arrays);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,coutries);
        pays_origine.setAdapter(dataAdapter);

    }

    class MyTask extends AsyncTask<String, String, String> {

        @Override
        //S'exécute avant le thread (doInBackground) où s'éxécute la requête.
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(true);
        }

        @Override
        //Où s'exécute la requête Http
        protected String doInBackground(String... voids) {
            try{
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(getString(R.string.url));

                // add header

                post.setHeader("Content-Type", "application/json");
                JSONObject obj = new JSONObject();
                obj.put(getString(R.string.put_firstname), me.getPrenom());
                obj.put(getString(R.string.put_name), me.getNom());
                obj.put(getString(R.string.put_phone), me.getTelephone());
                obj.put(getString(R.string.put_email), me.getEmail());
                obj.put("password", me.getPass());
                obj.put("sexe", me.getGenre());
                obj.put("createdby",me.getPrenom());

//                .....
                StringEntity entity = new StringEntity(obj.toString());

                post.setEntity(entity);

                HttpResponse response = client.execute(post);
                System.out.println("Post parameters : " + post.getEntity());
                System.out.println("Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println(result.toString());
                return result.toString();
            } catch (Exception e){

            }
            return null;
        }


        @Override
        //S'éxécute après le thread (doInBackground) où s'éxécute la requête Http
        protected void onPostExecute(String theResponse) {
            super.onPostExecute(theResponse);
            showProgressDialog(false);
            Toast.makeText(RegisterActivity.this, R.string.inscription_ok, Toast.LENGTH_LONG).show(); //le pop up
            startActivity(new Intent(RegisterActivity.this, ListView.class));

        }
    }

    ProgressDialog progressDialog;
    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(this.getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }
}